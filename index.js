//Array Methods
// JS has built-in function and methods for arrays.

//mutator method
//mutator -

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

//arrayName.push() - add the data in the last array
console.log(fruits);

fruits.push("Mango");
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log(fruits);

//arrayname.pop
//remove array element to the end of the array
console.log(fruits);
let removedFruit = fruits.pop();
console.log(fruits);
console.log(removedFruit);

//arrayname.unshift() - add item at the first element
console.log(fruits);
fruits.unshift("Lime", "Banana");
console.log(fruits);


//arrayname.shift() - remove at the first element
console.log('SHIFTTTT');
console.log(fruits);
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);


//splice() - getting range of an array
//return a new array;
//arrayname.splice(startIndex, deleteCount, elementsToBeAdde);
console.log(fruits);
let wow = fruits.splice(1, 2, "Lime", "Cherry");
console.log(wow);
console.log(fruits);

//arrange alphanumeric
//arrayname.sort()
console.log('---');
console.log(fruits);
fruits.sort();
console.log(fruits);

//arrange in reverse order
//arrayname.reverse();
console.log('original');
console.log(fruits);
fruits.reverse();
console.log(fruits);

//Non-mutator methods
//checking content only
let countries = ["LS", "PH", "CAN", "SG", "PH"];

//indexOf() - return the first matching element
//if not found, return -1;
//Syntax -> arrayName.indexOf(searchValue, fromIndex);
let first = countries.indexOf("PH");
console.log(first);

let second = countries.indexOf('WW');
console.log(second);

// lastIndexOf()
//return index number of the last matching element in an array
//Syntax -> arrayName.lastIndexOf(searchValue, fromIndex);
let lastIn = countries.lastIndexOf("PH");
console.log(lastIn);

//getting the index nmber starting from specified index
//Syntax -> arrayName.indexOf(searchValue, fromIndex);
let lastIndexStart = countries.lastIndexOf("PH", 3);
console.log(lastIndexStart);


//slice()
//return a portion or slices elements from an array and return
//a new array
//arrayName.slice(startIndex, endIndex); return new array
//from start up to endd
let sliceAndArrayA = countries.slice(2);
console.log(sliceAndArrayA);
console.log(countries);

let sliceB = countries.slice(1, 3);
console.log(sliceB);
console.log(countries);

//slice negative numbers
let sliceC = countries.slice(-3);
console.log(sliceC);
console.log(countries);


// toString();
//return an array as a string separated by commans
//arrayName.toString();
let stringD = countries.toString();
console.log(stringD);


//concat()
//combines two arrays and return the combined result
//arrayA.concat(arrayB);
let arr1 = ["Drink html", "eat js"];
let arr2 = ["inhale css", "breathe ssas"];
let arr3 = ["get git", "be node"];

let combi1 = arr1.concat(arr2, arr3);
console.log(combi1);

//combine array with elements
let combi2 = arr1.concat("smell exp", "throw  react");
console.log(combi2);

// join();
// return an array as a string speratted by specified separator stiring
// SYNTAX - arrayname.join('separatorString');

let user1 = ["A", "B", "C", "D"];
console.log(user1.join('-'));


//iteration method
//forEach
//similar to for loop that iterate on each array element
//syntax array.forEach(function(individualLement){

//});
user1.forEach(function(task){
    console.log(task);
});

//using forEach with conditional statement
let newLetter = [];

combi2.forEach(function(task){

    if (task.length > 10){
        newLetter.push(task);
    }
});

console.log(combi2);
console.log(newLetter);

//map() return something into an array
//this is useful for performing tasks where mutating
//changing the elements are required
//syntax -> let/const resultArray = arrayName.map(functin(parameter){

//})
//original array does not affected by map
//new array is return and stored in a vriable

let numbersArr = [1,2,3,4,5];
let numberMap = numbersArr.map(function(numberArr){
    return numberArr * numberArr;
});
console.log("original: ");
console.log(numbersArr);

console.log("result : ");
console.log(numberMap);

//map() vs forEach()
let numberForEach = numbersArr.forEach(function(number){
    return number * number;
})
console.log(numberForEach);
//return undefined
//forEach(). loops overall items in the array
//as for map, but forEach() does not return new array.
//forEach() - return in string;, originil only
//map() - return new array, duplicate


//every() - parang &&
//checks if all elements in an array meet the given codnition
//syntax -> let/const resultArray = arrayName.every(function(parameter){

//});

let allValid = numbersArr.every(function(number){
    return number < 3;
});
console.log(allValid);


//some(); parang ||
//checks if at least one lement in array meets the condition
 //syntax -> let/const resultArray = arrayName.some(function(parameter){

//})

let someValid = numbersArr.some(function(number){
    return number < 2;
});
console.log(someValid);


//filter();
//return new array that contains element which meet the given
//condition
//return  an empty rray if no element were found
 //syntax -> let/const resultArray = arrayName.filter(function(parameter){

//})

let filterVald = numbersArr.filter(function(number){
    return number < 3;
});
console.log(filterVald);    

//includes();
//boolean kung meron or wala yung passed parameter
//syntax -> arrayName.inclides(parameter);

let products = ["mouse", "kaey", "monitor"];
let productFound1 = products.includes("mouse");
console.log(productFound1); //true

let productFound2 = products.includes("WW");
console.log(productFound2); //F


let filteredProd = products.filter(function(product){
    return product.toLowerCase().includes("a");
});

console.log(filteredProd);


//reduce()
//evaluates elements from left to right and returns/
//reduces the array into a single value.
//syntax -> let/const resultArr = array.reduce(function(accumulator, current value){
//  operation
//})

//x - accumulator ; y - current value
let iteration = 0;
let reduceArr = numbersArr.reduce(function(x, y){
    console.warn('current iteration: ' + ++iteration);
    console.log('accumulator: ' + x);
    console.log('current value: ' + y)

    //the operation to reduce the array into single value
    return x + y;
});

console.log(reduceArr);


let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x, y){
    return x + " " + y;
});

console.log(reducedJoin);